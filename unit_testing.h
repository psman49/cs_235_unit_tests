/*
* main.cpp
*
*  Created by Lee Barney on 10/20/18.
*  Copyright (c) 2018 Lee Barney
*  Permission is hereby granted, free of charge, to any person obtaining a
*  copy of this software and associated documentation files (the "Software"),
*  to deal in the Software without restriction, including without limitation the
*  rights to use, copy, modify, merge, publish, distribute, sublicense,
*  and/or sell copies of the Software, and to permit persons to whom the
*  Software is furnished to do so, subject to the following conditions:

*  The above copyright notice and this permission notice shall be
*  included in all copies or substantial portions of the Software.
*
*  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
*  SOFTWARE.
*/

#ifndef unit_testing_delete_h
#define unit_testing_delete_h

#include <math.h>
#include <iostream>
#include <new>

//DON'T EDIT THIS CODE!! IF YOU DO, YOU WILL HATE LIFE.

#ifdef UNIT_TESTING
//This global int variable is used to track calls to delete for unit testing purposes only.
int unit_testing_delete_call_counter = 0;

int asserts_passed = 0;
const char* theTestSetName = NULL;
bool passedFlag = true;

extern int asserts_existing;

void startTestSet(const char* aTestSetName){
    if(theTestSetName != NULL){
        if (passedFlag) {
            std::cout<<"Passed"<<std::endl;
        }
    }
    passedFlag = true;
    std::cout<<"Testing "<<aTestSetName<<"...";
    theTestSetName = aTestSetName;
}
void generateTestingReport(){
    if (passedFlag) {
        std::cout<<"Passed"<<std::endl;
    }
    std::cout<<"\n\nPercentage of Tests Passed:  "<<floor(100*(float)asserts_passed/asserts_existing)<<"%"<<std::endl;
}

#undef assert
void assertTrue(bool passed, int aLineNumber)
{
    if (!passed)
    {
        if(passedFlag){
            std::cout<<std::endl;
        }
        passedFlag = false;
        std::cout<<"\tfailed on line "<<aLineNumber<<std::endl;
    }
    else{
        asserts_passed++;
    }
}

//overloading the delete operator globally.
bool deletingArray = false;
#ifdef __APPLE__
void operator delete(void * p) _NOEXCEPT
#else
void operator delete(void * p)
#endif
{
    free(p);
    if (!deletingArray) {
        unit_testing_delete_call_counter++;
    }
}

#ifdef __APPLE__
void operator delete[]( void* ptr)  _NOEXCEPT
#else
void operator delete[]( void* ptr)
#endif
{
    if(ptr != NULL){
        deletingArray = true;
        unit_testing_delete_call_counter++;
        free(ptr);
        deletingArray = false;
    }
}

void * operator new(size_t size) throw(std::bad_alloc)
{
    void * p = malloc(size);
    return p;
}
#endif /* UNIT_TESTING */
#endif /* unit_testing_delete_h */
